import {IronLibsCommon as __} from "ironlibs-base";
import * as Gpio  from "./pigpio";


export module IronLibsRaspiPin
{

    //##################################################################################################################
    //######  MODULE-SCOPE PRIVATE STUFF
    //##################################################################################################################

    let GpioLib : typeof Gpio;
    if (process.platform != "win32")
        GpioLib = require('pigpio');

    /**
     *  IMPORTANT: the state representation and related data of each pin MUST reside in a SHARED space,
     *  NOT relied to each Pin instance (which can get lost upon all application modules)
     */
    let PinsTable : { [s : string] : IPinPrivateDescriptor; } = {};



    interface IPinPrivateDescriptor
    {
        Mode : PIN_MODE;

        Blink? : BlinkBehaviour;

        SwPwmData? : {
            LoopHandle : number
        };

        HwPwmData? : {};

        GPio : Gpio.Gpio;
    }

    class BlinkBehaviour
    {
        private stopped : boolean;
        private currVal : PIN_VALUE;

        constructor(private pinNumber : number,
                    public highTime : number,
                    public lowTime : number)
        {
            let self = this;
            let descriptor = PinsTable[pinNumber];

            let clbk = function()
            {
                if (self.stopped)
                    return;

                if (self.currVal == PIN_VALUE.HIGH)
                {
                    //WRITE the value
                    descriptor.GPio.digitalWrite(PIN_VALUE.HIGH);
                    //raspiPin.setHigh(pinNumber);
                    //rpio.write(pinNumber, PIN_VALUE.HIGH);

                    if (globalOptions.Debug)
                        __.Log("  >> Pin " + self.pinNumber + " Blink " + __.GetEnumKey(PIN_VALUE, self.currVal));

                    self.currVal = PIN_VALUE.LOW;
                    setTimeout(clbk, self.highTime);      //stay HIGH for highTime ms
                }
                else
                {
                    //WRITE the value
                    descriptor.GPio.digitalWrite(PIN_VALUE.LOW);
                    //raspiPin.setLow(pinNumber);
                    //rpio.write(pinNumber, PIN_VALUE.LOW);

                    if (globalOptions.Debug)
                        __.Log("  >> Pin " + self.pinNumber + " Blink " + __.GetEnumKey(PIN_VALUE, self.currVal));

                    self.currVal = PIN_VALUE.HIGH;
                    setTimeout(clbk, self.lowTime);      //stay HIGH for highTime ms
                }
            };

            this.stopped = false;
            this.currVal = PIN_VALUE.HIGH;

            if (globalOptions.Debug)
                __.Log("  >> Pin " + pinNumber + " started Blink (" +
                    __.GetEnumKey(PIN_VALUE, PIN_VALUE.HIGH) + ": " + highTime + ", " +
                    __.GetEnumKey(PIN_VALUE, PIN_VALUE.LOW) + ": " + lowTime + ")");

            clbk();
        }


        /**
         * Make the loop DIE
         */
        public Stop() : void
        {
            this.stopped = true;

            if (globalOptions.Debug)
                __.Log("  >> Pin " + this.pinNumber + " stopped Blink");
        }
    }


    function Throw(msg : string)
    {
        if (globalOptions.UseCustomExceptions)
            throw new OperationException(msg);
        else
            throw new Error(msg);
    }


    let globalOptions : IOptions =     //with the DEFAULT VALUES
            {
                UseCustomExceptions : true,
                Debug               : true
            };

    let initialized = false;



//##################################################################################################################
//######  MODULE EXPORTS  ##########################################################################################
//##################################################################################################################

    export interface IOptions
    {
        UseCustomExceptions? : boolean;
        Debug? : boolean;
    }

    export declare class Error
    {
        public name : string;
        public message : string;
        public stack : string;

        constructor(message? : string);
    }

    export class OperationException extends Error
    {
        constructor(public message : string)
        {
            super(message);
            this.name = 'OperationException';
            this.message = message;
            this.stack = (<any>new Error()).stack;
        }

        toString()
        {
            return this.name + ': ' + this.message;
        }
    }


    export enum PIN_MODE
    {
        UNDEFINED     = 0,
        SIMPLE_OUTPUT = 1,
        INPUT         = 2,
        SW_PWM        = 3,
        HW_PWM        = 4,
        BLINK         = 5,
        FADE          = 6
    }

    export enum PIN_VALUE
    {
        LOW  = 0,
        HIGH = 1
    }


    /**
     * Ensures that the GpioLib library is initialized.
     * Multiple calls are correctly handled
     *
     * @returns {boolean} TRUE upon success, FALSE otherwise
     */
    export function Initialize(/*setRealtimeScheduling : boolean = true*/) : boolean
    {
        if (initialized)
            return true;

        initialized = __.IsNotNull(GpioLib); //raspiPin.initialize();

        // if(initialized && setRealtimeScheduling)
        //     raspiPin.setRealtimeScheduling();

        return initialized;
    }


    /**
     * Invokes the GpioLib library terminations (releases internal TCP ports for remote control, etc...)
     * To be invoked after Initialize
     *
     * @returns {boolean} TRUE upon success, FALSE otherwise
     */
    export function Terminate() : boolean
    {
        if (!initialized)
            return false;

        GpioLib.terminate();
        initialized = false;

        return true;
    }


    /**
     * Merges the specified options to the main ones
     * @param newOptions a subset of IOptions
     */
    export function SetGlobalOptions(newOptions : IOptions) : void
    {
        if (!__.IsNotNullObject(newOptions))
            Throw("SetGlobalOptions accepts an object");

        globalOptions = __.MergeObj(globalOptions, newOptions);
    }


    export class Pin
    {
        constructor(private num : number, initialMode : PIN_MODE = PIN_MODE.UNDEFINED)
        {
            if (__.IsNull(PinsTable [this.num]))
            {
                if (__.IsNull(GpioLib))
                {
                    __.Log("  >> ERROR (unsupported in Windows) Can't initialize Pin " + this.num + " initialized as " + __.GetEnumKey(PIN_MODE, initialMode));
                    return;
                }
                //FIRST time we used this pin: create its main table entry
                let pinMode = {mode : GpioLib.Gpio.OUTPUT};
                if (initialMode == PIN_MODE.INPUT)
                    pinMode = {mode : GpioLib.Gpio.INPUT};

                if (globalOptions.Debug)
                    __.Log("  >> Creating Pin " + this.num + " instance");

                PinsTable [this.num] = {
                    Mode : initialMode,
                    GPio : new GpioLib.Gpio(this.num, pinMode)
                };
                //
                // if (initialMode != PIN_MODE.UNDEFINED)
                // {
                //     raspiPin.setPinMode(this.num, initialMode == PIN_MODE.INPUT ? 0 : 1);
                // }

                if (globalOptions.Debug)
                    __.Log("  >> Pin " + this.num + " initialized as " + __.GetEnumKey(PIN_MODE, initialMode));
            }
        }

        //##################################################################################################################
        //######  PRIVATE STUFF
        // ############################################################################################
        // ##################################################################################################################



        //##################################################################################################################
        //######  PUBLIC STUFF  ############################################################################################
        //##################################################################################################################


        public GetMode() : PIN_MODE { return PinsTable [this.num].Mode; }

        public GetNumber() : number { return this.num; }



        /**
         * Clears the internal pin status STOPPING all its activities and write LOW
         * (to permit a change in its mode)
         */
        public Reset() : void
        {
            if (__.IsNull(GpioLib))
            {
                __.Log("  >> ERROR (unsupported in Windows) Reset Pin " + this.num);
                return;
            }

            this.SilentReset(true);

            if (globalOptions.Debug)
                __.Log("  >> Pin " + this.num + " configured as " + __.GetEnumKey(PIN_MODE, PinsTable [this.num].Mode) + " after Reset");
        }


        private SilentReset(writeLow : boolean) : void
        {
            if (__.IsNull(GpioLib))
            {
                __.Log("  >> ERROR (unsupported in Windows) SilentReset Pin " + this.num);
                return;
            }

            let descriptor = PinsTable [this.num];

            descriptor.Mode = PIN_MODE.UNDEFINED;

            //DESTROY any activity
            //TODO: check and stop timers
            if (__.IsNotNull(descriptor.Blink))
            {
                descriptor.Blink.Stop();
                descriptor.Blink = null;
            }

            if (writeLow)
            {
                //WRITE the value
                descriptor.GPio.digitalWrite(PIN_VALUE.LOW);
                //raspiPin.setLow(this.num);
                //rpio.write(this.num, rpio.LOW);

                if (globalOptions.Debug)
                    __.Log("  >> Pin " + this.num + " Write " + __.GetEnumKey(PIN_VALUE, PIN_VALUE.LOW));
            }
        }


        public Write(value : PIN_VALUE) : void
        {
            if (__.IsNull(GpioLib))
            {
                __.Log("  >> ERROR (unsupported in Windows) Write Pin " + this.num + " VALUE " + __.GetEnumKey(PIN_VALUE, value));
                return;
            }

            let descriptor : IPinPrivateDescriptor = PinsTable[this.num];

            //TYPES check (mainly for non-TypeScript user code)
            if (!__.IsNumber(value))
                Throw("Write accepts a numeric value");

            //MODE check and set
            let mode = PinsTable [this.num].Mode;
            if (mode == PIN_MODE.INPUT)
            {
                Throw("Pin " + this.num + " was in mode " + __.GetEnumKey(PIN_MODE, PIN_MODE.INPUT) + " and now is requested to be " + __.GetEnumKey(PIN_MODE, PIN_MODE.SIMPLE_OUTPUT));
            }
            else if (mode != PIN_MODE.SIMPLE_OUTPUT)
            {
                this.SilentReset(false);     //DESTROY any activity

                PinsTable [this.num].Mode = PIN_MODE.SIMPLE_OUTPUT;
                if (globalOptions.Debug)
                    __.Log("  >> Pin " + this.num + " configured as " + __.GetEnumKey(PIN_MODE, PinsTable [this.num].Mode));
                descriptor.GPio.mode(GpioLib.Gpio.OUTPUT);
                //raspiPin.setPinMode(this.num, 1);
            }

            //WRITE the value
            descriptor.GPio.digitalWrite(value);

            if (globalOptions.Debug)
                __.Log("  >> Pin " + this.num + " Write " + __.GetEnumKey(PIN_VALUE, value));
        }

        //
        // public StartPwm(hertz : number, percentage : number) : boolean
        // {
        //     //TYPES check (mainly for non-TypeScript user code)
        //     if (!__.IsNumber(hertz) || !__.IsNumber(percentage))
        //         Throw("StartPwm accepts 2 numeric values");
        //
        //     //MODE check and set
        //     var mode = Pin.PinsTable[this.num].Mode;
        //     if (mode != PIN_MODE.SW_PWM)
        //     {
        //         Throw("Pin " + this.num + " was in mode " + __.GetEnumKey(PIN_MODE, mode) + " and now is requested to be " + __.GetEnumKey(PIN_MODE, PIN_MODE.SW_PWM));
        //     }
        //
        //     var ret : boolean = raspiPin.startPwm(this.num, hertz, percentage);
        //
        //     var limitedPerc = __.LimitNumberPrecision(percentage, 2);
        //
        //     if (globalOptions.Debug)
        //         __.Log("  >> Pin " + this.num + " START PWM\thertz: " + hertz + "hz\tpercentage: " + limitedPerc + "%");
        //
        //     return ret;
        // }
        //
        //
        public StartHwPwm(percentage : number, freq : number = 1000) : void
        {
            if (__.IsNull(GpioLib))
            {
                __.Log("  >> ERROR (unsupported in Windows) StartHwPwm Pin " + this.num + " PERC " + percentage + " FREQ " + freq);
                return;
            }

            //TYPES check (mainly for non-TypeScript user code)
            if (!__.IsNumber(percentage))
                Throw("StartPwm accepts a numeric value");

            if(percentage < 0)
                percentage = 0;
            if(percentage > 100)
                percentage = 100;

            //MODE check and set
            let descriptor : IPinPrivateDescriptor = PinsTable[this.num];
            if (descriptor.Mode != PIN_MODE.HW_PWM)
                Throw("Pin " + this.num + " was in mode " + __.GetEnumKey(PIN_MODE, descriptor.Mode) + " and now is requested to be " + __.GetEnumKey(PIN_MODE, PIN_MODE.HW_PWM));

            let range = 1000000;    //for HW PWM this is fixed
            let duty = Math.round((range * percentage) / 100);

            if (globalOptions.Debug)
                __.Log("  >> Pin " + this.num + " START HARDWARE PWM\tpercentage: " +  __.LimitNumberPrecision(percentage, 2) + "% (duty: " + duty + ")");

            descriptor.GPio.hardwarePwmWrite(freq, duty);   //duty must be an integer
        }

        //
        // public ChangePwm(hertz : number, percentage : number) : void
        // {
        //     //TYPES check (mainly for non-TypeScript user code)
        //     if (!__.IsNumber(hertz) || !__.IsNumber(percentage))
        //         Throw("ChangePwm accepts 2 numeric values");
        //
        //     //MODE check and set
        //     var mode = Pin.PinsTable[this.num].Mode;
        //     if (mode != PIN_MODE.SW_PWM)
        //     {
        //         Throw("Pin " + this.num + " was in mode " + __.GetEnumKey(PIN_MODE, mode) + " and now is requested to be " + __.GetEnumKey(PIN_MODE, PIN_MODE.SW_PWM));
        //     }
        //
        //     raspiPin.changePwm(this.num, hertz, percentage);
        //
        //     var limitedPerc = __.LimitNumberPrecision(percentage, 2);
        //
        //     if (globalOptions.Debug)
        //         __.Log("  >> Pin " + this.num + " CHANGE PWM\thertz: " + hertz + "hz\tpercentage: " + limitedPerc + "%");
        // }

        //
        // public ChangeHwPwm(percentage : number) : void
        // {
        //     //TYPES check (mainly for non-TypeScript user code)
        //     if (!__.IsNumber(percentage))
        //         Throw("ChangePwm accepts 1 numeric value");
        //
        //     //MODE check and set
        //     var mode = Pin.PinsTable[this.num].Mode;
        //     if (mode != PIN_MODE.HW_PWM)
        //     {
        //         Throw("Pin " + this.num + " was in mode " + __.GetEnumKey(PIN_MODE, mode) + " and now is requested to be " + __.GetEnumKey(PIN_MODE, PIN_MODE.HW_PWM));
        //     }
        //
        //     raspiPin.changeHwPwm(this.num, percentage);
        //
        //
        //     if (globalOptions.Debug)
        //     {
        //         var limitedPerc = __.LimitNumberPrecision(percentage, 2);
        //         __.Log("  >> Pin " + this.num + " CHANGE HARDWARE PWM\tpercentage: " + limitedPerc + "%");
        //     }
        // }
        //
        //
        // public StopPwm() : void
        // {
        //     //MODE check and set
        //     var mode = Pin.PinsTable[this.num].Mode;
        //     if (mode != PIN_MODE.SW_PWM)
        //     {
        //         Throw("Pin " + this.num + " was in mode " + __.GetEnumKey(PIN_MODE, mode) + " and now is requested to be " + __.GetEnumKey(PIN_MODE, PIN_MODE.SW_PWM));
        //     }
        //
        //     raspiPin.stopPwm(this.num);
        //
        //     if (globalOptions.Debug)
        //         __.Log("  >> Pin " + this.num + " STOP PWM");
        // }
        //
        //
        // public StopHwPwm() : void
        // {
        //     //MODE check and set
        //     var mode = Pin.PinsTable[this.num].Mode;
        //     if (mode != PIN_MODE.HW_PWM)
        //     {
        //         Throw("Pin " + this.num + " was in mode " + __.GetEnumKey(PIN_MODE, mode) + " and now is requested to be " + __.GetEnumKey(PIN_MODE, PIN_MODE.HW_PWM));
        //     }
        //
        //     raspiPin.stopHwPwm(this.num);
        //
        //     if (globalOptions.Debug)
        //         __.Log("  >> Pin " + this.num + " STOP HARDWARE PWM");
        // }



        public Read() : PIN_VALUE
        {
            if (__.IsNull(GpioLib))
            {
                __.Log("  >> ERROR (unsupported in Windows) Read Pin " + this.num);
                return PIN_VALUE.LOW;
            }

            //MODE check and set
            let descriptor = PinsTable [this.num];

            if (descriptor.Mode != PIN_MODE.INPUT)
            {
                Throw("Pin " + this.num + " was in mode " + __.GetEnumKey(PIN_MODE, descriptor.Mode) + " and now is requested to be " + __.GetEnumKey(PIN_MODE, PIN_MODE.INPUT));
            }

            //READ the value
            let value : PIN_VALUE = descriptor.GPio.digitalRead();

            if (globalOptions.Debug)
                __.Log("  >> Pin " + this.num + " Read value: " + __.GetEnumKey(PIN_VALUE, value));

            return value;
        }



        public Blink(highTime : number, lowTime : number = highTime) : void
        {
            if (__.IsNull(GpioLib))
            {
                __.Log("  >> ERROR (unsupported in Windows) Blink Pin " + this.num);
                return;
            }

            //TYPES check (mainly for non-TypeScript user code)
            if (!__.IsNumber(highTime) || !__.IsNumber(lowTime))
                Throw("Blink accepts at least one numeric value");

            //MODE check and set
            let mode = PinsTable [this.num].Mode;
            if (mode == PIN_MODE.INPUT)
            {
                Throw("Pin " + this.num + " was in mode " + __.GetEnumKey(PIN_MODE, PIN_MODE.INPUT) + " and now is requested to be " + __.GetEnumKey(PIN_MODE, PIN_MODE.BLINK));
            }
            else if (mode != PIN_MODE.BLINK)
            {
                PinsTable [this.num].Mode = PIN_MODE.BLINK;
                if (globalOptions.Debug)
                    __.Log("  >> Pin " + this.num + " configured as " + __.GetEnumKey(PIN_MODE, PinsTable [this.num].Mode));

            }

            //DESTROY any activity (also a previous blink in order to avoid to wait for its end to start the new one....)
            this.SilentReset(false);

            //Start the blink
            PinsTable [this.num].Blink = new BlinkBehaviour(this.num, highTime, lowTime);
        }

    }

}
