# IronLibs

Ironpipp's useful utilities for both nodejs, browser with jQuery or Angular.

This repo holds raspberry-only utilities.

## Features
- Fully typed
- by default no dependencies are needed. Are required ONLY when used
    - **pigpio** for IronLibsRaspiPin

So install proper packages based on your necessities.

### How to build and test
     > npm run build
