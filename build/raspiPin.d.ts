export declare module IronLibsRaspiPin {
    interface IOptions {
        UseCustomExceptions?: boolean;
        Debug?: boolean;
    }
    class Error {
        name: string;
        message: string;
        stack: string;
        constructor(message?: string);
    }
    class OperationException extends Error {
        message: string;
        constructor(message: string);
        toString(): string;
    }
    enum PIN_MODE {
        UNDEFINED = 0,
        SIMPLE_OUTPUT = 1,
        INPUT = 2,
        SW_PWM = 3,
        HW_PWM = 4,
        BLINK = 5,
        FADE = 6
    }
    enum PIN_VALUE {
        LOW = 0,
        HIGH = 1
    }
    /**
     * Ensures that the GpioLib library is initialized.
     * Multiple calls are correctly handled
     *
     * @returns {boolean} TRUE upon success, FALSE otherwise
     */
    function Initialize(): boolean;
    /**
     * Invokes the GpioLib library terminations (releases internal TCP ports for remote control, etc...)
     * To be invoked after Initialize
     *
     * @returns {boolean} TRUE upon success, FALSE otherwise
     */
    function Terminate(): boolean;
    /**
     * Merges the specified options to the main ones
     * @param newOptions a subset of IOptions
     */
    function SetGlobalOptions(newOptions: IOptions): void;
    class Pin {
        private num;
        constructor(num: number, initialMode?: PIN_MODE);
        GetMode(): PIN_MODE;
        GetNumber(): number;
        /**
         * Clears the internal pin status STOPPING all its activities and write LOW
         * (to permit a change in its mode)
         */
        Reset(): void;
        private SilentReset;
        Write(value: PIN_VALUE): void;
        StartHwPwm(percentage: number, freq?: number): void;
        Read(): PIN_VALUE;
        Blink(highTime: number, lowTime?: number): void;
    }
}
