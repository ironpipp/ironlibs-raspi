"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.IronLibsRaspiPin = void 0;
const ironlibs_base_1 = require("ironlibs-base");
var IronLibsRaspiPin;
(function (IronLibsRaspiPin) {
    //##################################################################################################################
    //######  MODULE-SCOPE PRIVATE STUFF
    //##################################################################################################################
    let GpioLib;
    if (process.platform != "win32")
        GpioLib = require('pigpio');
    /**
     *  IMPORTANT: the state representation and related data of each pin MUST reside in a SHARED space,
     *  NOT relied to each Pin instance (which can get lost upon all application modules)
     */
    let PinsTable = {};
    class BlinkBehaviour {
        constructor(pinNumber, highTime, lowTime) {
            this.pinNumber = pinNumber;
            this.highTime = highTime;
            this.lowTime = lowTime;
            let self = this;
            let descriptor = PinsTable[pinNumber];
            let clbk = function () {
                if (self.stopped)
                    return;
                if (self.currVal == PIN_VALUE.HIGH) {
                    //WRITE the value
                    descriptor.GPio.digitalWrite(PIN_VALUE.HIGH);
                    //raspiPin.setHigh(pinNumber);
                    //rpio.write(pinNumber, PIN_VALUE.HIGH);
                    if (globalOptions.Debug)
                        ironlibs_base_1.IronLibsCommon.Log("  >> Pin " + self.pinNumber + " Blink " + ironlibs_base_1.IronLibsCommon.GetEnumKey(PIN_VALUE, self.currVal));
                    self.currVal = PIN_VALUE.LOW;
                    setTimeout(clbk, self.highTime); //stay HIGH for highTime ms
                }
                else {
                    //WRITE the value
                    descriptor.GPio.digitalWrite(PIN_VALUE.LOW);
                    //raspiPin.setLow(pinNumber);
                    //rpio.write(pinNumber, PIN_VALUE.LOW);
                    if (globalOptions.Debug)
                        ironlibs_base_1.IronLibsCommon.Log("  >> Pin " + self.pinNumber + " Blink " + ironlibs_base_1.IronLibsCommon.GetEnumKey(PIN_VALUE, self.currVal));
                    self.currVal = PIN_VALUE.HIGH;
                    setTimeout(clbk, self.lowTime); //stay HIGH for highTime ms
                }
            };
            this.stopped = false;
            this.currVal = PIN_VALUE.HIGH;
            if (globalOptions.Debug)
                ironlibs_base_1.IronLibsCommon.Log("  >> Pin " + pinNumber + " started Blink (" +
                    ironlibs_base_1.IronLibsCommon.GetEnumKey(PIN_VALUE, PIN_VALUE.HIGH) + ": " + highTime + ", " +
                    ironlibs_base_1.IronLibsCommon.GetEnumKey(PIN_VALUE, PIN_VALUE.LOW) + ": " + lowTime + ")");
            clbk();
        }
        /**
         * Make the loop DIE
         */
        Stop() {
            this.stopped = true;
            if (globalOptions.Debug)
                ironlibs_base_1.IronLibsCommon.Log("  >> Pin " + this.pinNumber + " stopped Blink");
        }
    }
    function Throw(msg) {
        if (globalOptions.UseCustomExceptions)
            throw new OperationException(msg);
        else
            throw new Error(msg);
    }
    let globalOptions = {
        UseCustomExceptions: true,
        Debug: true
    };
    let initialized = false;
    class OperationException extends Error {
        constructor(message) {
            super(message);
            this.message = message;
            this.name = 'OperationException';
            this.message = message;
            this.stack = new Error().stack;
        }
        toString() {
            return this.name + ': ' + this.message;
        }
    }
    IronLibsRaspiPin.OperationException = OperationException;
    let PIN_MODE;
    (function (PIN_MODE) {
        PIN_MODE[PIN_MODE["UNDEFINED"] = 0] = "UNDEFINED";
        PIN_MODE[PIN_MODE["SIMPLE_OUTPUT"] = 1] = "SIMPLE_OUTPUT";
        PIN_MODE[PIN_MODE["INPUT"] = 2] = "INPUT";
        PIN_MODE[PIN_MODE["SW_PWM"] = 3] = "SW_PWM";
        PIN_MODE[PIN_MODE["HW_PWM"] = 4] = "HW_PWM";
        PIN_MODE[PIN_MODE["BLINK"] = 5] = "BLINK";
        PIN_MODE[PIN_MODE["FADE"] = 6] = "FADE";
    })(PIN_MODE = IronLibsRaspiPin.PIN_MODE || (IronLibsRaspiPin.PIN_MODE = {}));
    let PIN_VALUE;
    (function (PIN_VALUE) {
        PIN_VALUE[PIN_VALUE["LOW"] = 0] = "LOW";
        PIN_VALUE[PIN_VALUE["HIGH"] = 1] = "HIGH";
    })(PIN_VALUE = IronLibsRaspiPin.PIN_VALUE || (IronLibsRaspiPin.PIN_VALUE = {}));
    /**
     * Ensures that the GpioLib library is initialized.
     * Multiple calls are correctly handled
     *
     * @returns {boolean} TRUE upon success, FALSE otherwise
     */
    function Initialize( /*setRealtimeScheduling : boolean = true*/) {
        if (initialized)
            return true;
        initialized = ironlibs_base_1.IronLibsCommon.IsNotNull(GpioLib); //raspiPin.initialize();
        // if(initialized && setRealtimeScheduling)
        //     raspiPin.setRealtimeScheduling();
        return initialized;
    }
    IronLibsRaspiPin.Initialize = Initialize;
    /**
     * Invokes the GpioLib library terminations (releases internal TCP ports for remote control, etc...)
     * To be invoked after Initialize
     *
     * @returns {boolean} TRUE upon success, FALSE otherwise
     */
    function Terminate() {
        if (!initialized)
            return false;
        GpioLib.terminate();
        initialized = false;
        return true;
    }
    IronLibsRaspiPin.Terminate = Terminate;
    /**
     * Merges the specified options to the main ones
     * @param newOptions a subset of IOptions
     */
    function SetGlobalOptions(newOptions) {
        if (!ironlibs_base_1.IronLibsCommon.IsNotNullObject(newOptions))
            Throw("SetGlobalOptions accepts an object");
        globalOptions = ironlibs_base_1.IronLibsCommon.MergeObj(globalOptions, newOptions);
    }
    IronLibsRaspiPin.SetGlobalOptions = SetGlobalOptions;
    class Pin {
        constructor(num, initialMode = PIN_MODE.UNDEFINED) {
            this.num = num;
            if (ironlibs_base_1.IronLibsCommon.IsNull(PinsTable[this.num])) {
                if (ironlibs_base_1.IronLibsCommon.IsNull(GpioLib)) {
                    ironlibs_base_1.IronLibsCommon.Log("  >> ERROR (unsupported in Windows) Can't initialize Pin " + this.num + " initialized as " + ironlibs_base_1.IronLibsCommon.GetEnumKey(PIN_MODE, initialMode));
                    return;
                }
                //FIRST time we used this pin: create its main table entry
                let pinMode = { mode: GpioLib.Gpio.OUTPUT };
                if (initialMode == PIN_MODE.INPUT)
                    pinMode = { mode: GpioLib.Gpio.INPUT };
                if (globalOptions.Debug)
                    ironlibs_base_1.IronLibsCommon.Log("  >> Creating Pin " + this.num + " instance");
                PinsTable[this.num] = {
                    Mode: initialMode,
                    GPio: new GpioLib.Gpio(this.num, pinMode)
                };
                //
                // if (initialMode != PIN_MODE.UNDEFINED)
                // {
                //     raspiPin.setPinMode(this.num, initialMode == PIN_MODE.INPUT ? 0 : 1);
                // }
                if (globalOptions.Debug)
                    ironlibs_base_1.IronLibsCommon.Log("  >> Pin " + this.num + " initialized as " + ironlibs_base_1.IronLibsCommon.GetEnumKey(PIN_MODE, initialMode));
            }
        }
        //##################################################################################################################
        //######  PRIVATE STUFF
        // ############################################################################################
        // ##################################################################################################################
        //##################################################################################################################
        //######  PUBLIC STUFF  ############################################################################################
        //##################################################################################################################
        GetMode() { return PinsTable[this.num].Mode; }
        GetNumber() { return this.num; }
        /**
         * Clears the internal pin status STOPPING all its activities and write LOW
         * (to permit a change in its mode)
         */
        Reset() {
            if (ironlibs_base_1.IronLibsCommon.IsNull(GpioLib)) {
                ironlibs_base_1.IronLibsCommon.Log("  >> ERROR (unsupported in Windows) Reset Pin " + this.num);
                return;
            }
            this.SilentReset(true);
            if (globalOptions.Debug)
                ironlibs_base_1.IronLibsCommon.Log("  >> Pin " + this.num + " configured as " + ironlibs_base_1.IronLibsCommon.GetEnumKey(PIN_MODE, PinsTable[this.num].Mode) + " after Reset");
        }
        SilentReset(writeLow) {
            if (ironlibs_base_1.IronLibsCommon.IsNull(GpioLib)) {
                ironlibs_base_1.IronLibsCommon.Log("  >> ERROR (unsupported in Windows) SilentReset Pin " + this.num);
                return;
            }
            let descriptor = PinsTable[this.num];
            descriptor.Mode = PIN_MODE.UNDEFINED;
            //DESTROY any activity
            //TODO: check and stop timers
            if (ironlibs_base_1.IronLibsCommon.IsNotNull(descriptor.Blink)) {
                descriptor.Blink.Stop();
                descriptor.Blink = null;
            }
            if (writeLow) {
                //WRITE the value
                descriptor.GPio.digitalWrite(PIN_VALUE.LOW);
                //raspiPin.setLow(this.num);
                //rpio.write(this.num, rpio.LOW);
                if (globalOptions.Debug)
                    ironlibs_base_1.IronLibsCommon.Log("  >> Pin " + this.num + " Write " + ironlibs_base_1.IronLibsCommon.GetEnumKey(PIN_VALUE, PIN_VALUE.LOW));
            }
        }
        Write(value) {
            if (ironlibs_base_1.IronLibsCommon.IsNull(GpioLib)) {
                ironlibs_base_1.IronLibsCommon.Log("  >> ERROR (unsupported in Windows) Write Pin " + this.num + " VALUE " + ironlibs_base_1.IronLibsCommon.GetEnumKey(PIN_VALUE, value));
                return;
            }
            let descriptor = PinsTable[this.num];
            //TYPES check (mainly for non-TypeScript user code)
            if (!ironlibs_base_1.IronLibsCommon.IsNumber(value))
                Throw("Write accepts a numeric value");
            //MODE check and set
            let mode = PinsTable[this.num].Mode;
            if (mode == PIN_MODE.INPUT) {
                Throw("Pin " + this.num + " was in mode " + ironlibs_base_1.IronLibsCommon.GetEnumKey(PIN_MODE, PIN_MODE.INPUT) + " and now is requested to be " + ironlibs_base_1.IronLibsCommon.GetEnumKey(PIN_MODE, PIN_MODE.SIMPLE_OUTPUT));
            }
            else if (mode != PIN_MODE.SIMPLE_OUTPUT) {
                this.SilentReset(false); //DESTROY any activity
                PinsTable[this.num].Mode = PIN_MODE.SIMPLE_OUTPUT;
                if (globalOptions.Debug)
                    ironlibs_base_1.IronLibsCommon.Log("  >> Pin " + this.num + " configured as " + ironlibs_base_1.IronLibsCommon.GetEnumKey(PIN_MODE, PinsTable[this.num].Mode));
                descriptor.GPio.mode(GpioLib.Gpio.OUTPUT);
                //raspiPin.setPinMode(this.num, 1);
            }
            //WRITE the value
            descriptor.GPio.digitalWrite(value);
            if (globalOptions.Debug)
                ironlibs_base_1.IronLibsCommon.Log("  >> Pin " + this.num + " Write " + ironlibs_base_1.IronLibsCommon.GetEnumKey(PIN_VALUE, value));
        }
        //
        // public StartPwm(hertz : number, percentage : number) : boolean
        // {
        //     //TYPES check (mainly for non-TypeScript user code)
        //     if (!__.IsNumber(hertz) || !__.IsNumber(percentage))
        //         Throw("StartPwm accepts 2 numeric values");
        //
        //     //MODE check and set
        //     var mode = Pin.PinsTable[this.num].Mode;
        //     if (mode != PIN_MODE.SW_PWM)
        //     {
        //         Throw("Pin " + this.num + " was in mode " + __.GetEnumKey(PIN_MODE, mode) + " and now is requested to be " + __.GetEnumKey(PIN_MODE, PIN_MODE.SW_PWM));
        //     }
        //
        //     var ret : boolean = raspiPin.startPwm(this.num, hertz, percentage);
        //
        //     var limitedPerc = __.LimitNumberPrecision(percentage, 2);
        //
        //     if (globalOptions.Debug)
        //         __.Log("  >> Pin " + this.num + " START PWM\thertz: " + hertz + "hz\tpercentage: " + limitedPerc + "%");
        //
        //     return ret;
        // }
        //
        //
        StartHwPwm(percentage, freq = 1000) {
            if (ironlibs_base_1.IronLibsCommon.IsNull(GpioLib)) {
                ironlibs_base_1.IronLibsCommon.Log("  >> ERROR (unsupported in Windows) StartHwPwm Pin " + this.num + " PERC " + percentage + " FREQ " + freq);
                return;
            }
            //TYPES check (mainly for non-TypeScript user code)
            if (!ironlibs_base_1.IronLibsCommon.IsNumber(percentage))
                Throw("StartPwm accepts a numeric value");
            if (percentage < 0)
                percentage = 0;
            if (percentage > 100)
                percentage = 100;
            //MODE check and set
            let descriptor = PinsTable[this.num];
            if (descriptor.Mode != PIN_MODE.HW_PWM)
                Throw("Pin " + this.num + " was in mode " + ironlibs_base_1.IronLibsCommon.GetEnumKey(PIN_MODE, descriptor.Mode) + " and now is requested to be " + ironlibs_base_1.IronLibsCommon.GetEnumKey(PIN_MODE, PIN_MODE.HW_PWM));
            let range = 1000000; //for HW PWM this is fixed
            let duty = Math.round((range * percentage) / 100);
            if (globalOptions.Debug)
                ironlibs_base_1.IronLibsCommon.Log("  >> Pin " + this.num + " START HARDWARE PWM\tpercentage: " + ironlibs_base_1.IronLibsCommon.LimitNumberPrecision(percentage, 2) + "% (duty: " + duty + ")");
            descriptor.GPio.hardwarePwmWrite(freq, duty); //duty must be an integer
        }
        //
        // public ChangePwm(hertz : number, percentage : number) : void
        // {
        //     //TYPES check (mainly for non-TypeScript user code)
        //     if (!__.IsNumber(hertz) || !__.IsNumber(percentage))
        //         Throw("ChangePwm accepts 2 numeric values");
        //
        //     //MODE check and set
        //     var mode = Pin.PinsTable[this.num].Mode;
        //     if (mode != PIN_MODE.SW_PWM)
        //     {
        //         Throw("Pin " + this.num + " was in mode " + __.GetEnumKey(PIN_MODE, mode) + " and now is requested to be " + __.GetEnumKey(PIN_MODE, PIN_MODE.SW_PWM));
        //     }
        //
        //     raspiPin.changePwm(this.num, hertz, percentage);
        //
        //     var limitedPerc = __.LimitNumberPrecision(percentage, 2);
        //
        //     if (globalOptions.Debug)
        //         __.Log("  >> Pin " + this.num + " CHANGE PWM\thertz: " + hertz + "hz\tpercentage: " + limitedPerc + "%");
        // }
        //
        // public ChangeHwPwm(percentage : number) : void
        // {
        //     //TYPES check (mainly for non-TypeScript user code)
        //     if (!__.IsNumber(percentage))
        //         Throw("ChangePwm accepts 1 numeric value");
        //
        //     //MODE check and set
        //     var mode = Pin.PinsTable[this.num].Mode;
        //     if (mode != PIN_MODE.HW_PWM)
        //     {
        //         Throw("Pin " + this.num + " was in mode " + __.GetEnumKey(PIN_MODE, mode) + " and now is requested to be " + __.GetEnumKey(PIN_MODE, PIN_MODE.HW_PWM));
        //     }
        //
        //     raspiPin.changeHwPwm(this.num, percentage);
        //
        //
        //     if (globalOptions.Debug)
        //     {
        //         var limitedPerc = __.LimitNumberPrecision(percentage, 2);
        //         __.Log("  >> Pin " + this.num + " CHANGE HARDWARE PWM\tpercentage: " + limitedPerc + "%");
        //     }
        // }
        //
        //
        // public StopPwm() : void
        // {
        //     //MODE check and set
        //     var mode = Pin.PinsTable[this.num].Mode;
        //     if (mode != PIN_MODE.SW_PWM)
        //     {
        //         Throw("Pin " + this.num + " was in mode " + __.GetEnumKey(PIN_MODE, mode) + " and now is requested to be " + __.GetEnumKey(PIN_MODE, PIN_MODE.SW_PWM));
        //     }
        //
        //     raspiPin.stopPwm(this.num);
        //
        //     if (globalOptions.Debug)
        //         __.Log("  >> Pin " + this.num + " STOP PWM");
        // }
        //
        //
        // public StopHwPwm() : void
        // {
        //     //MODE check and set
        //     var mode = Pin.PinsTable[this.num].Mode;
        //     if (mode != PIN_MODE.HW_PWM)
        //     {
        //         Throw("Pin " + this.num + " was in mode " + __.GetEnumKey(PIN_MODE, mode) + " and now is requested to be " + __.GetEnumKey(PIN_MODE, PIN_MODE.HW_PWM));
        //     }
        //
        //     raspiPin.stopHwPwm(this.num);
        //
        //     if (globalOptions.Debug)
        //         __.Log("  >> Pin " + this.num + " STOP HARDWARE PWM");
        // }
        Read() {
            if (ironlibs_base_1.IronLibsCommon.IsNull(GpioLib)) {
                ironlibs_base_1.IronLibsCommon.Log("  >> ERROR (unsupported in Windows) Read Pin " + this.num);
                return PIN_VALUE.LOW;
            }
            //MODE check and set
            let descriptor = PinsTable[this.num];
            if (descriptor.Mode != PIN_MODE.INPUT) {
                Throw("Pin " + this.num + " was in mode " + ironlibs_base_1.IronLibsCommon.GetEnumKey(PIN_MODE, descriptor.Mode) + " and now is requested to be " + ironlibs_base_1.IronLibsCommon.GetEnumKey(PIN_MODE, PIN_MODE.INPUT));
            }
            //READ the value
            let value = descriptor.GPio.digitalRead();
            if (globalOptions.Debug)
                ironlibs_base_1.IronLibsCommon.Log("  >> Pin " + this.num + " Read value: " + ironlibs_base_1.IronLibsCommon.GetEnumKey(PIN_VALUE, value));
            return value;
        }
        Blink(highTime, lowTime = highTime) {
            if (ironlibs_base_1.IronLibsCommon.IsNull(GpioLib)) {
                ironlibs_base_1.IronLibsCommon.Log("  >> ERROR (unsupported in Windows) Blink Pin " + this.num);
                return;
            }
            //TYPES check (mainly for non-TypeScript user code)
            if (!ironlibs_base_1.IronLibsCommon.IsNumber(highTime) || !ironlibs_base_1.IronLibsCommon.IsNumber(lowTime))
                Throw("Blink accepts at least one numeric value");
            //MODE check and set
            let mode = PinsTable[this.num].Mode;
            if (mode == PIN_MODE.INPUT) {
                Throw("Pin " + this.num + " was in mode " + ironlibs_base_1.IronLibsCommon.GetEnumKey(PIN_MODE, PIN_MODE.INPUT) + " and now is requested to be " + ironlibs_base_1.IronLibsCommon.GetEnumKey(PIN_MODE, PIN_MODE.BLINK));
            }
            else if (mode != PIN_MODE.BLINK) {
                PinsTable[this.num].Mode = PIN_MODE.BLINK;
                if (globalOptions.Debug)
                    ironlibs_base_1.IronLibsCommon.Log("  >> Pin " + this.num + " configured as " + ironlibs_base_1.IronLibsCommon.GetEnumKey(PIN_MODE, PinsTable[this.num].Mode));
            }
            //DESTROY any activity (also a previous blink in order to avoid to wait for its end to start the new one....)
            this.SilentReset(false);
            //Start the blink
            PinsTable[this.num].Blink = new BlinkBehaviour(this.num, highTime, lowTime);
        }
    }
    IronLibsRaspiPin.Pin = Pin;
})(IronLibsRaspiPin = exports.IronLibsRaspiPin || (exports.IronLibsRaspiPin = {}));
//# sourceMappingURL=raspiPin.js.map